<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sign Up</title>
</head>
<body>
	<form method="post" action="/welcome">
        @csrf
	<h1> Buat Account Baru </h1>
	<h3> Sign Up Form</h3>
	<label for="fname">First Name</label><br><br>
		<input type="text" name="firstname"><br><br>
	<label for="lname">Last Name</label><br><br>
		<input type="text" name="lastname"><br><br>
	<label for="gender">Gender</label><br><br>
		<input type="radio" name="gender" value="M"> Male<br>
		<input type="radio" name="gender" value="F"> Female<br>
		<input type="radio" name="gender" 	value="O"> Other<br><br>
	<label for="nationality">Nationality:</label><br><br>
		<select>
			<option value="Indonesia">Indonesian</option>
			<option value="Singapore">Singaporean</option>
			<option value="Malaysia">Malaysian</option>
			<option value="Australia">Australian</option>
		</select><br><br>
	<label for="language">Language Spoken:</label><br><br>
		<input type="checkbox" name="language">Bahasa Indonesia<br>
		<input type="checkbox" name="language">English<br>
		<input type="checkbox" name="language">Other<br><br>
	<label for="bio">Bio</label><br><br>
		<textarea name="bio" rows="10" cols="30"></textarea><br><br>
	<button type="submit">Sign Up</button>
</form>
</body>
</html>




