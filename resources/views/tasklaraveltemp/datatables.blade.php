@extends('template.master')

@section('title1')
    Sanbercode
@endsection

@section('title2')
    DATA TABLES
@endsection

@section('company')
    Sanbercode
@endsection

@section('username')
    Username
@endsection

@section('content')
    @include('tasklaraveltemp.datatables-content')
@endsection

@push('style')
    
@endpush

@push('scripts')
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush