@extends('template.master')

@section('title1')
    Sanbercode
@endsection

@section('title2')
    TABLE
@endsection

@section('company')
    Sanbercode
@endsection

@section('username')
    Username
@endsection

@section('content')
    @include('tasklaraveltemp.table-content')
@endsection

@push('style')
    
@endpush

@push('scripts')
    
@endpush