@extends('template.layoutcrud')

@section('title1')
    Sanbercode
@endsection

@section('title2')
    Edit
@endsection

@section('company')
    Sanbercode
@endsection

@section('username')
    Username
@endsection

@section('content')
<div class="ml-3 mt-2">
    <div class="card card-primary">
      <div class="card-header">
      <h3 class="card-title">Edit Questions ID {{$question->id}}</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="/questions/{{$question->id}}" method="POST">
          @csrf
          @method('PUT')
        <div class="card-body">
          <div class="form-group">
              <label for="id">ID</label>
              <input type="number" class="form-control" name="id" value="{{ old('id', $question->id) }}" id="id" placeholder="Enter ID">
              @error('id')
                  <div class="alert alert-danger">Fill the blank.</div>
              @enderror
          </div>
          <div class="form-group">
              <label for="title">Descriptions</label>
              <input type="text" class="form-control" name="title" value="{{ old('title', $question->title) }}" id="title" placeholder="Descriptions">
              @error('title')
                  <div class="alert alert-danger">Fill the blank.</div>
              @enderror
          </div>
          <div class="form-group">
              <label for="content">Questions</label>
              <input type="text" class="form-control" name="content" value="{{ old('content', $question->content) }}" id="content" placeholder="Write Down Your Questions">
              @error('content')
                  <div class="alert alert-danger">Fill the blank.</div>
              @enderror
          </div>
          <div class="form-group">
              <label for="create_date">Updated Date</label>
              <input type="date" class="form-control" name="create_date" value="{{ old('create_date', $question->create_date) }}" id="create_date">
              @error('create_date')
               <div class="alert alert-danger">Fill the blank.</div>
              @enderror
          </div>
        <!-- /.card-body -->
        <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
@endsection