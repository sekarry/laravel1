@extends('template.layoutcrud')

@section('title1')
Sanbercode
@endsection

@section('title2')
    Show
@endsection

@section('company')
    Sanbercode
@endsection

@section('username')
    Username
@endsection

@section('content')
    <div class="ml-3 mt-3">
        <h4> {{$question->id}} </h4>
        <h4> {{$question->title}} </h4>
        <h4> {{$question->content}} </h4>
        <h4> {{$question->create_date}} </h4>
    </div>
@endsection