@extends('template.layoutcrud')

@section('title1')
    Sanbercode
@endsection

@section('title2')
    Data
@endsection

@section('company')
    Sanbercode
@endsection

@section('username')
    Username
@endsection

@section('content')
<div class="mt-2 ml-3">
    @if (session('success'))
        <div class="alert alert-success"> {{session('success')}} </div>
    @endif
    <a href="/questions/create" class="btn btn-info mb-1">NEW</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID</th>
                <th scope="col">Request Date</th>
                <th scope="col">Content</th>
                <th scope="col" style="display: flex">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($questions as $key=>$value)
                <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$value->id}} </td>
                    <td> {{$value->create_date}} </td>
                    <td> {{$value->content}} </td>
                    <td style="display: flex;">
                        <a href="/questions/{{$value->id}}" class="btn btn-info btn-sm mr-1">Show</a>
                        <a href="/questions/{{$value->id}}/edit" class="btn btn-default btn-sm mr-1">Edit</a>
                        <form action="/questions/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>               
            @empty
                <tr colspan="4">
                    <td>No Data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection