<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignProfileIdToLikeDislikeQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_dislike_questions', function (Blueprint $table) {
            $table->unsignedBigInteger('profile_id');

            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->unsignedBigInteger('like_dislike_id');

            $table->foreign('like_dislike_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments_on_questions', function (Blueprint $table) {
            $table->dropForeign('profile_id');
            $table->dropColumn('profile_id');
            $table->dropForeign('like_dislike_id');
            $table->dropColumn('like_dislike_id');
        });
    }
}
