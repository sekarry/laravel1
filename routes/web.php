<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');


Route::get('/master', function() {
    return view('template.master');
});
/*---------------------------------------pekan 3 hari 4--------------------------------------------------*/

/* Disoal seharusnya '/' saja, tapi karena diwork ini sudah ada jadi saya ganti '/table'*/
Route::get('/table', function() {
    return view('tasklaraveltemp.table');
});

Route::get('/datatables', function() {
    return view('tasklaraveltemp.datatables');
});
/*----------------------------------------pekan 3 hari 5-------------------------------------------------*/

// Route::get('/questions', 'QuestionsController@index')->name('questions.index');

// Route::get('/questions/create', 'QuestionsController@create');

// Route::post('/questions', 'QuestionsController@store');

// Route::get('/questions/{questions_id}', 'QuestionsController@show');

// Route::get('/questions/{questions_id}/edit', 'QuestionsController@edit');

// Route::put('/questions/{questions_id}', 'QuestionsController@update');

// Route::delete('/questions/{questions_id}', 'QuestionsController@destroy');

Route::resource('questions', 'QuestionsController');