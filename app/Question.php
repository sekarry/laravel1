<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //protected $table = "questions";

    protected $fillable = ["id", "title", "content", "create_date"];

    //protected $guarded = []; yg didalam array bakal keblacklist sehingga gak bisa diisi lalu akan muncul error, 
    //kalo dikosongin boleh aja biar langsung dan gak pelru mass $fillable kayaknya
}
