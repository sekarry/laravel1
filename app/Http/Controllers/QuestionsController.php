<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;

class QuestionsController extends Controller
{
    public function index() {
        //$questions = DB::table('questions')->get();
        // dd($questions);

        $questions = Question::all();
        //dd($questions);
        return view('crudblade.index', compact('questions'));
    }

    public function create() {
        return view('crudblade.create');
    }

    public function store(Request $request) {
        /*dd($request->all());*/
        $request->validate([
            "id" => 'required',
            "title" => 'required',
            "content" => 'required',
            "create_date" => 'required'
        ]);
        // $query = DB::table('questions')->insert([
        //     "id" => $request['id'],
        //     "title" => $request['title'],
        //     "content" => $request['content'],
        //     "create_date" => $request['create_date']
        // ]);

        // $question = new Question;
        // $question->id = $request["id"];
        // $question->title = $request["title"];
        // $question->content = $request["content"];
        // $question->create_date = $request["create_date"];
        // $question->save(); 

        $question = Question::create([
            "id" => $request["id"],
            "title" => $request["title"],
            "content" => $request["content"],
            "create_date" => $request["create_date"]
        ]);

        return redirect('/questions')->with('success', 'Your request has been sent!');
    }

    public function show($id) {
        // $question = DB::table('questions')->where('id', $id)->first();
        $question = Question::find($id);
        return view('crudblade.show', compact('question'));
    }

    public function edit($id) {
        $question = DB::table('questions')->where('id', $id)->first();
        return view('crudblade.edit', compact('question'));
    }

    public function update($id, Request $request) {
        $request->validate([
            "id" => 'required',
            "title" => 'required',
            "content" => 'required',
            "create_date" => 'required'
        ]);

        // $query = DB::table('questions')
        //             ->where('id', $id)
        //             ->update([
        //                 'id' => $request['id'],
        //                 'title' => $request['title'],
        //                 'content' => $request['content'],
        //                 'create_date' => $request['create_date']
        //             ]);

        $update = Question::where('id', $id)->update([
            "id" => $request["id"],
            "title" => $request["title"],
            "content" => $request["content"],
            "create_date" => $request["create_date"]
        ]);

        return redirect('/questions')->with('success', 'Your request has been updated!');
    }

    public function destroy($id) {
        //$query = DB::table('questions')->where('id', $id)->delete();

        Question::destroy($id);
        return redirect('/questions')->with('success', 'Request has been deleted:(');
    }
};
